var path = require('path');
var rootPath = path.normalize(__dirname + '/../../');


module.exports={
    development:{
        db: 'mongodb://localhost/multivision',
        port: process.env.PORT || 3030,
        rootPath: rootPath
    },
    production: {
        db: 'mongodb://koenvg:multivision@ds037407.mongolab.com:37407/multivision',
        port: process.env.PORT || 80,
        rootPath: rootPath
    }
};