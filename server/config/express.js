var express = require('express'),
    lessMiddleware = require('less-middleware'),
    passport = require('passport');


module.exports = function(app, config){
    app.configure(function(){
        app.set('views', config.rootPath + '/server/views');
        app.set('view engine', 'jade');
        app.use(express.logger('dev'));
        app.use(express.cookieParser());
        app.use(express.bodyParser());
        app.use(express.session({secret: 'unicorns and rainbows'}));
        app.use(passport.initialize());
        app.use(passport.session());
        app.use(lessMiddleware(
            {
                dest: config.rootPath + '/public/css',
                src: config.rootPath + '/public/less',
                compress: true
            }
        ));
        app.use(express.static(config.rootPath + '/public'));
    });

};

