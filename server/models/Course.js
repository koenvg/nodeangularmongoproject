var mongoose = require('mongoose');

var courseSchema = mongoose.Schema({
    title: {type: String, required: '{PATH} is required!'},
    featured: {type: Boolean, required: '{PATH} is required!'},
    published: {type: Date, required: '{PATH} is required!'},
    tags: [String]
});

var Course = mongoose.model('Course', courseSchema);

function createDefaultCourses() {
    Course.find({}).exec(function(err, collection) {
        if(collection.length === 0) {
            Course.create({
                title: 'test',
                featured: true,
                published: new Date('10/5/2013'),
                tags: ['nodejs']
            });
            Course.create({
                title: 'banaan',
                featured: true,
                published: new Date('10/5/2017'),
                tags: ['nodejs']
            });
            Course.create({
                title: 'test2',
                featured: true,
                published: new Date('10/5/2014'),
                tags: ['nodejs']
            });
        }
    })
}
exports.createDefaultCourses = createDefaultCourses;