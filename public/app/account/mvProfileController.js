angular.module('app').controller('mvProfileController',function($scope, mvAuth, mvIdentity, mvNotifier){

    $scope.email = mvIdentity.currentUser.username;
    $scope.fname = mvIdentity.currentUser.firstName;
    $scope.lname = mvIdentity.currentUser.lastName;

    $scope.update = function() {
        var newUserDate = {
            username: $scope.email,
            firstName: $scope.fname,
            lastName: $scope.lname
        };

        if($scope.password && $scope.password.length > 0) {
            newUserDate.password = $scope.password;
        }

        mvAuth.updateCurrentUser(newUserDate).then(function() {
            mvNotifier.notify('Your user account has been updated');
        }, function(reason) {
           mvNotifier.error(reason);
        });
    }
});