angular.module('app').controller('mvSignupController', function($scope, mvAuth, mvNotifier, mvUser, $location){

    $scope.signup = function(){
        var newUserDate = {
            username: $scope.email,
            password: $scope.password,
            firstName: $scope.fname,
            lastName: $scope.lname
        };

        mvAuth.createUser(newUserDate).then(function() {
            mvNotifier.notify('User account created!');
            $location.path('/');
        }, function(reason) {
            mvNotifier.error(reason);
        });
    };
});