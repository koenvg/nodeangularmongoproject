angular.module('app').controller('mvNavBarLoginController',
    function($scope, $http, mvIdentity, mvNotifier, mvAuth, $location){

        $scope.identity = mvIdentity;
        $scope.signin = function(username, password){
            mvAuth.authenticateUser(username, password).then(function(success){
               if(success){
                   mvNotifier.notify('logged in!');
               } else{
                   mvNotifier.notify('TUTA TUTA TUTA TUTA the police is here!');
               }
            });
        }

        $scope.signout = function(){
            mvAuth.logoutUser().then(function() {
                $scope.username = '';
                $scope.password = '';
                mvNotifier.notify('You have successfully signed out!');
                $location.path('/');
            });
        }
    }
);