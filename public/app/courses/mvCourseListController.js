angular.module('app').controller('mvCourseListController', function($scope, mvCachedCourses) {

    $scope.courses = mvCachedCourses.query();

    $scope.sortOptions = [{
        value: 'title',
        text: 'Sort By Title'
    }, {
        value: 'published',
        text: 'Sort by publish date'
    }];

    $scope.sortOrder = $scope.sortOptions[0].value;
});